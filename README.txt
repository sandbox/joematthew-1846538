Beagle Anti-Fraud
=================

This module enables Beagle API Anti-Fraud protection for eWay customers. Beagle uses GEO-IP rules to block or
flag potential fraudulent transactions. Rules are declared in MYeWAY and notifications are sent via e-mail for
any transactions that do not pass the Anti-Fraud rules. Sample code and sandbox testing are available from MYeWAY.
This module builds on the Rapid 3.0 method of connection to eWay for Ubercart.

This module development and maintenance is sponsored by Powerful CMS, in partnership with eWay for technical guidance
and validation.

Support for Drupal 6.x isn't planned.

There you can try out a demonstration: http://www.sterlingcurrency.com.au
